/**
 * Created by Justyna on 2017-02-16.
 */
public class TriathlonStrategyMain {
    public static void main(String [] args){
        Context competition = new Context(new Swim());
        competition.executeTriathlonStrategy();

        competition = new Context(new Ride());
        competition.executeTriathlonStrategy();

        competition = new Context(new Run());
        competition.executeTriathlonStrategy();
    }
}
