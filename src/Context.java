/**
 * Created by Justyna on 2017-02-16.
 */
public class Context {
    private TriathlonStrategy triathlonStrategy;

    public Context(TriathlonStrategy triathlonStrategy){
        this.triathlonStrategy = triathlonStrategy;
    }

    public void executeTriathlonStrategy() {
       triathlonStrategy.move();
    }
}
